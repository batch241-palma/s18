// alert ("Hello World")

/* no.3 */
function addTwoNumbers(addNum1, addNum2) {
	let sum = addNum1 + addNum2;
	console.log("Display the sum of numbers " + addNum1 + " and " + addNum2 + " is:")
	 console.log(sum)

}
 /* no.5 */
 addTwoNumbers(5, 15);
 

 /* no.4 */
 function subtractTwoNumbers(subNum1, subNum2) {
	let difference = subNum1 - subNum2;
	console.log("The difference of " + subNum1 + " and " + subNum2 + " is: ")
	console.log(difference)
}
 /* no.6 */
 subtractTwoNumbers(20, 5);



 /* no.7 */
 function multiplyTwoNumbers(mulNum1, mulNum2) {

	let productOutput = mulNum1 * mulNum2;
	console.log("The product of " + mulNum1 + " and " + mulNum2 + " is: ")
	return productOutput 

}
/* no. 9 */
let product = multiplyTwoNumbers(50, 10);
console.log(product);



 /* no.8 */
 function divideTwoNumbers(divNum1, divNum2) {

	let quotientOutput = (divNum1 / divNum2)
	console.log("The quotient of " + divNum1 + " and " + divNum2 + " is: ")
	return quotientOutput 

}
/* no. 10 */
let quotient = divideTwoNumbers(50, 10);
console.log(quotient);


/* no. 11 */
function areaOfCircle(radius) {

	let circleAreaOutput = (3.1416 * (radius ** 2))
	console.log("The result of getting the area of a circle with " + radius + " radius: " )
	return circleAreaOutput 

}
/* no. 12 */
let circleArea = areaOfCircle(15);
console.log(circleArea);

/* no. 13 */
function totalAverageOfFourNumbers(aveNum1, aveNum2, aveNum3, aveNum4) {

	let theAverageOfFour = ((aveNum1 + aveNum2 + aveNum3 + aveNum4) / 4)
	console.log("The total average of four numbers: " + aveNum1 + " , " + aveNum2 + " , " + aveNum3 + " , " + " and " + aveNum4 + " is: ")
	return theAverageOfFour 

}
/* no. 14 */
let averageVar = totalAverageOfFourNumbers(20,40,60,80);
console.log(averageVar);


/* no. 15 */
function pasingPercentage(yourScore, totalScore) {

	let isPassed = (((yourScore / totalScore) * 100) >= 75)
	console.log("Is " + yourScore + "/" +totalScore + " a passing score?")
	return isPassed 

}
/* no. 16*/
let isPassingScore = pasingPercentage(38,50);
console.log(isPassingScore);
